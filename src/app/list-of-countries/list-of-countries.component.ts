import { Component, OnDestroy, OnInit } from '@angular/core';
import { Country } from '../shared/country.model';
import { CountriesService } from '../shared/countries.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-of-countries',
  templateUrl: './list-of-countries.component.html',
  styleUrls: ['./list-of-countries.component.css']
})
export class ListOfCountriesComponent implements OnInit, OnDestroy {

  countries: Country[] = [];
  countriesChangeSubscription!: Subscription;
  countriesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private countryService: CountriesService) {}

  ngOnInit(): void {
    this.countries = this.countryService.getCountries();
    this.countriesChangeSubscription = this.countryService.countriesChange.subscribe((countries: Country[]) => {
      this.countries = countries;
    });
    this.countriesFetchingSubscription = this.countryService.countriesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.countryService.fetchCountries();
  }

  ngOnDestroy() {
    this.countriesChangeSubscription.unsubscribe();
  }
}
