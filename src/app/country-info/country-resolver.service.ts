import { Injectable } from '@angular/core';
import { Country } from '../shared/country.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CountriesService } from '../shared/countries.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CountryResolverService implements Resolve<Country> {

  constructor(private countryService: CountriesService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const alphaCode = <string>route.params['alphaCode'];
     return this.countryService.fetchCountry(alphaCode);
  }
}
