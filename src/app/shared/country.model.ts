export class Country {
  constructor(
    public name: string,
    public alpha3Code: string,
    public capital: string,
    public region: string,
    public population: number,
  ) {}
}
