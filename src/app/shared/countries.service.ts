import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Country } from './country.model';
import { Subject } from 'rxjs';

@Injectable()

export class CountriesService {
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private countries: Country[] = [];

  getCountries() {
    return this.countries.slice();
  }

  fetchCountries() {
    this.countriesFetching.next(true);
    this.http.get<{ [id: string]: Country }>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const countryData = result[id];
          return new Country(countryData.name, countryData.alpha3Code, countryData.capital, countryData.region, countryData.population)
        });
      }))
      .subscribe(countries => {
        this.countries = countries;
        this.countriesChange.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, () => {
        this.countriesFetching.next(false);
      });
  }


  fetchCountry(alphaCode: string) {
    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${alphaCode}`)
      .pipe(map(result => {
          return new Country(result.name, alphaCode, result.capital, result.region, result.population);
        })
      )
  }
}
