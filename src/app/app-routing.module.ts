import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryInfoComponent } from './country-info/country-info.component';
import { CountryResolverService } from './country-info/country-resolver.service';


const routes: Routes = [
  {path: 'country/:alphaCode', component: CountryInfoComponent, resolve: {
    country: CountryResolverService
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
